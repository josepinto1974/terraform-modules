

########################
######################################

data "aws_availability_zones" "available" {}

 locals {
#cidr= tonumber(var.cidr_vpc)
oct1= tonumber(split(".", var.cidr_vpc)[0])
#teste0= local.teste1 + 40
oct2= split(".", var.cidr_vpc)[1]

oct33= split(".", var.cidr_vpc)[2]
oct3= local.oct33 + 20
oct44= split(".", var.cidr_vpc)[3]

oct4= split("/", local.oct44)[0]


oct5= 26


oct1priv= tonumber(split(".", var.cidr_vpc)[0])
#teste0= local.teste1 + 40
oct2priv= split(".", var.cidr_vpc)[1]

oct3privn= split(".", var.cidr_vpc)[2]
oct3priv= local.oct3privn + 10
oct44priv= split(".", var.cidr_vpc)[3]
oct4priv= split("/", local.oct44priv)[0]

#oct5priv= split("/", "10.20.0.0/16")[1]

oct5priv= 24
}

#10.20.0.0/16 range 10.20.0.0 - 10.20.255.255
#https://mxtoolbox.com/subnetcalculator.aspx


###$$$$#$$$$$###########$$$$$$$$$$$$$$$$$$$$$$$$$$$




resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_vpc
  enable_dns_hostnames = true
  enable_dns_support = true


   tags = merge(
    { "Name" = var.network_objects_name },
    var.tags
   
  )

}

resource "aws_internet_gateway" "aws_internet_gateway1" {
  vpc_id = aws_vpc.vpc.id

 #tags = var.tags
}


##########$$$$#$$$$$$$$$$$$$$$$$$$$$$$$$$$$


/* BF duplicated  resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "rt_tags"
  }
} */





#######################
###########

locals {
  um  = "${local.oct1}.${local.oct2}.${local.oct3}.${local.oct4}/${local.oct5}"
}


resource "aws_subnet" "aws_subnet_public" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id      = aws_vpc.vpc.id
  #cidr_block = "10.20.${10+count.index}.0/24"
  cidr_block = "${local.oct1}.${local.oct2}.${local.oct3+count.index}.${local.oct4}/${local.oct5}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch = true
      tags = merge(
    { "Name" = "public_subnet"},
    { "kubernetes.io/role/elb" = "1"},
    var.tagssubnetk8public,
   
  )
}

##{ "Name" = "public_subnet".${data.aws_availability_zones.available.names[count.index]} },






##################



###################################################################################################
locals {
  dois  = "${local.oct1priv}.${local.oct2priv}.${local.oct3priv}.${local.oct4priv}/${local.oct5priv}"
  
}

resource "aws_subnet" "aws_subnet_private_subnet" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = aws_vpc.vpc.id
  #cidr_block = "10.20.${20+count.index}.0/24"
  cidr_block = "${local.oct1priv}.${local.oct2priv}.${local.oct3priv+count.index}.${local.oct4priv}/${local.oct5priv}"
  availability_zone= "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch = false
       tags = merge(
    { "Name" = "private_subnet" },
    { "kubernetes.io/role/internal-elb" = "1" },
    var.tagssubnetk8private,
   
  )
}

#####################################################################################################

#https://stackoverflow.com/questions/59097391/terraform-how-to-supply-attributes-of-resources-which-where-created-using-one-r

/*  resource "aws_db_subnet_group" "public" {
  name       = "my_database_subnet_group"
 subnet_ids = values(aws_subnet.public)[*].id

  tags = {
    Name = "My DB subnet group"
  }
}  */




################################################################################################

##https://dev.betterdoc.org/infrastructure/2020/02/04/setting-up-a-nat-gateway-on-aws-using-terraform.html
####https://thecloudbootcamp.com/pt/blog/aws/criando-vpc-com-uma-subnet-publica-utilizando-terraform/


# Criação da Rota Default para Acesso à Internet
##resource "aws_route" "tcb_blog_routetointernet" {
##  route_table_id            = aws_route_table.nat_gateway.id
##  destination_cidr_block    = "0.0.0.0/0"
 ## gateway_id                = aws_internet_gateway.nat_gateway.id


##}


###Routing

resource "aws_route_table" "public" {
   vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aws_internet_gateway1.id
  }

        tags = merge(
    { "Name" = "aws_route_table_public" },
    var.tags,
   
  )
}



###
resource "aws_eip" "eipgeneral" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc = true
   tags = {
    Name            = "vhub-tag"
    Owner           = "basma.kamal2@vodafone.com"
    TaggingVersion  = "V2.4"
    Environment     = "Stage"
    ManagedBy       = "basma.kamal2@vodafone.com"
    Confidentiality = "C2"
    SecurityZone    ="A"
    Project         = "vhub"
  }
}





resource "aws_nat_gateway" "aws_nat_gateway_1" {
 
  count = "${length(data.aws_availability_zones.available.names)}"
   allocation_id = aws_eip.eipgeneral[count.index].id
  subnet_id     = aws_subnet.aws_subnet_public[count.index].id

        tags = merge(
    { "Name" = var.network_objects_name },
    var.tags,
   
  )
}

 



resource "aws_route_table" "aws_route_table_private" {
  vpc_id = aws_vpc.vpc.id
  count = "${length(data.aws_availability_zones.available.names)}"
  route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.aws_nat_gateway_1[count.index].id
    }

#####
  route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.aws_nat_gateway_1[count.index].id
    }

         tags = merge(
    { "Name" = var.network_objects_name },
    var.tags,
   
  )
}


#############


##associate route tables and subnetes
####mar
#public subnetes

resource "aws_route_table_association" "public" {
#count = "${length(data.aws_availability_zones.available.names)}"
 count = "${length(data.aws_availability_zones.available.names)}"
   #subnet_id      = "${aws_subnet.aws_subnet_private_subnet[count.index].id}"
   # subnet_id      = "${aws_subnet.aws_subnet_public[count.index].id}"
   subnet_id      = element(aws_subnet.aws_subnet_public[*].id, count.index)
   #route_table_id = aws_route_table.public[count.index].id
   route_table_id = element(aws_route_table.public[*].id, count.index)
  

}



####
resource "aws_route_table_association" "private" {
#count = "${length(data.aws_availability_zones.available.names)}"
 count = "${length(data.aws_availability_zones.available.names)}"
   #subnet_id      = "${aws_subnet.aws_subnet_private_subnet[count.index].id}"  
    subnet_id      = "${aws_subnet.aws_subnet_private_subnet[count.index].id}"  
   route_table_id = aws_route_table.aws_route_table_private[count.index].id

} 


###ddwdd
/* 
resource "aws_route_table_association" "public_subnet_asso" {
 count = "${length(data.aws_availability_zones.available.names)}"
 subnet_id      = element(aws_subnet.aws_subnet_public[*].id, count.index)
 #route_table_id = aws_route_table.second_rt.id
 route_table_id = aws_route_table.public.id
}
 */



resource "aws_security_group" "default" {
  name        = "${var.environment}-default-sg"
  description = "Default SG to alllow traffic from the VPC"
  vpc_id = aws_vpc.vpc.id
  depends_on = [
    aws_vpc.vpc
  ]

    ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = "true"
    cidr_blocks = ["0.0.0.0/0"]
  }




       tags = merge(
    { "Name" = var.network_objects_name },
    var.tags,
   
  )
}

####################################################################################

locals {
val0= format("flowlogspolicy%s", var.project_name)
}
 
resource "aws_iam_policy" "policy" {
  name        = local.val0
  path        = "/"
  description = "AWSLoadBalancerControllerIAMPolicy"


  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Resource": "*"
    }
  ]
})   
}

locals {
val1= format("aws_iam_role_flowlogs%s", var.project_name)
}
resource "aws_iam_role" "flowlogs" {
  name = local.val1

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}



###### TO BE DONE CONFIG FLOW LOGS
##AWS documentation  https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html
##terra links  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/flow_log

#Removed temporaray#33 

resource "aws_flow_log" "example" {
  iam_role_arn    = aws_iam_role.example.arn
  log_destination = aws_cloudwatch_log_group.example.arn
  traffic_type    = "ALL"
  vpc_id = aws_vpc.vpc.id
}


 resource "aws_cloudwatch_log_group" "example" {
  #name = "var.cdwtcgrname"
  name = "${var.project_name}-TEST"
} 


locals {
val2= format("ipnameflg%s", var.project_name)
}
resource "aws_iam_role" "example" {
  name = local.val2
  

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "example" {
  name = "var.iamrflogs"
  role = aws_iam_role.example.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
##############



output aws_db_subnet_group_privateseseee {
 #value = ${aws_subnet.aws_subnet_private_subnet[*].id}

# value = values(aws_subnet.aws_subnet_private_subnet)[*].id
value = aws_subnet.aws_subnet_private_subnet.*.id

}
